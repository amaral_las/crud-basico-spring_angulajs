angular.module('usuariosApp.services')

.factory('usuariosFormSvc', ['$q', 'ENDPOINT_USUARIOS', function($q, ENDPOINT_USUARIOS){
     var getUsuario = function(id) {
            var q = $q.defer();
            $.ajax({
                url: ENDPOINT_USUARIOS + '/' + id,
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: {
                },
                type: 'GET',
                success: function(data) {
                    q.resolve(data) 
                },
                error: function(data) {
                		q.reject(data)
                }
            });

            return q.promise;
        };

        var insertUsuario = function(usuario) {
            var q = $q.defer();
            $.ajax({
                url: ENDPOINT_USUARIOS,
                dataType: 'json',
                data: JSON.stringify(usuario),
                contentType: "application/json; charset=utf-8",
                type: 'POST',
                success: function(data) {
                 	window.history.back()	
                },
                error: function(data) {
                		window.history.back()
                }
            });

            return q.promise;
        };
        
        var changeUsuario = function(usuario) {
            var q = $q.defer();
            $.ajax({
                url: ENDPOINT_USUARIOS + '/' + usuario.id,
                dataType: 'json',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(usuario),
                type: 'PUT',
                success: function(data) {
                		window.history.back()	
                },
                error: function(data) {
                		window.history.back()
                }
            });

            return q.promise;
        };

        var usuariosFormSvc = {
            getUsuario: getUsuario,
            insertUsuario: insertUsuario,
            changeUsuario: changeUsuario
        }
        
        return usuariosFormSvc;
    }
])