angular.module('usuariosApp.controllers')

.controller('UsuariosFormCtrl', function($scope, $stateParams, $window, $state, usuariosFormSvc) {
    $scope.usuario = {};
    $scope.noResults = false;
  
    
    $scope.inserOrUpdate = function(){
    		if($scope.usuario.id != null && $scope.usuario.id != ''){
    			$scope.updateUsuario()
    		}else{
    			$scope.insertUsuario()
    		}
    }

    $scope.updateUsuario = function() {
	    	if($scope.validateFields()){
		    	 usuariosFormSvc.changeUsuario($scope.usuario).then(function(response){
		    		 $window.history.back();
			})
	    	}
    }

    $scope.insertUsuario = function(){
	    	if($scope.validateFields()){
	       usuariosFormSvc.insertUsuario($scope.usuario).then(function(response){
	    	   console.log(response)
	    	   			$window.history.back();	
	       })
	    	}
    }
    
    $scope.validateFields = function(){
    		if($scope.usuario.name == "" || typeof $scope.usuario.name == "undefined" ){
    			window.alert('Campo nome é obrigatório');
    			return false;
    		}else if($scope.usuario.address == "" || typeof $scope.usuario.address == "undefined" ){
    			window.alert('Campo e-mail é obrigatório');
    			return false;
    		}else if($scope.usuario.cpf == "" || typeof $scope.usuario.cpf == "undefined" ){
    			window.alert('Campo cpf é obrigatório');
    			return false;
    		}
    		
    		return true;
    }

    $scope.getUsuario = function(){
      	usuariosFormSvc.getUsuario($stateParams.id).then(function(response){
      		if (response != null && response != "") {
      			$scope.usuario = response;
      		}
      	})
    }
    function init(){
	    	if($stateParams.id){
	            $scope.getUsuario()
	     }
    }
    
    init();
    
   
})