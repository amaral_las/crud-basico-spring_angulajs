angular.module('usuariosApp.controllers')

.controller('UsuariosCtrl', function ($scope, $stateParams, $window, $state, usuariosSvc){

    $scope.usuarios = [];

    $scope.noResults = true;

    $scope.deleteUsuario = function (id) {
     	usuariosSvc.deleteUsuario(id).then(function(response){
      		console.log(response)
      	})
    }

    $scope.getUsuarios = function(){
      	usuariosSvc.getUsuarios().then(function(response){
      		if (response != null && response.length > 0) {
      			$scope.usuarios = response;
      			$scope.noResults = false;
      		}
      	})
    }
    
    $scope.edit = function (codigo) {
    		$state.go("app.usuarios-edit",  {id: codigo});
    }
    
    $scope.destroy = function (codigo) {
    	    var id = codigo;
    	    var position = 0;
		if(window.confirm("Tem certeza que deseja excluir?")){
			usuariosSvc.deleteUsuario(codigo).then(function(response){
				$scope.usuarios.filter((o, i)=>{
					if(o.id == id){
						position = i;
					}
				})
				$scope.usuarios.splice(position, 1)
			},
			function(response){
				$scope.usuarios.filter((o, i)=>{
					if(o.id == id){
						position = i;
					}
				})
				$scope.usuarios.splice(position, 1);
				if($scope.usuarios.length == 0){
					$scope.noResults = true;
				}
			});
		}
    }
    
    $scope.novo = function () {
		$state.go("app.usuarios-create", {});
    }
    
    function init(){
     	$scope.getUsuarios();
    }
    
    init();
})
