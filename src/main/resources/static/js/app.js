
var backend = 'http://localhost:8080'

angular.module('usuariosApp', ['ngRoute', 'ui.router', 'usuariosApp.controllers', 'usuariosApp.services'])

.constant('ENDPOINT_USUARIOS', backend + '/usuarios')
   

.run(function ($rootScope, $location, $route) {

    })
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
        .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'views/layout.html',
                controller: 'AppCtrl'
            })
        .state('app.usuarios', {
            url: '/usuarios',
            views: {
                'usuariosAppContent': {
                    templateUrl: 'views/usuarios.html',
                    controller: 'UsuariosCtrl'
                }
            }
        })
        .state('app.usuarios-create', {
            url: '/usuarios-create',
            views: {
                'usuariosAppContent': {
                    templateUrl: 'views/usuarios_form.html',
                    controller: 'UsuariosFormCtrl'
                }
            }
        })
        .state('app.usuarios-edit', {
            url: '/usuarios-edit/:id',
            views: {
                'usuariosAppContent': {
                    templateUrl: 'views/usuarios_form.html',
                    controller: 'UsuariosFormCtrl'
                }
            }
        })
        $urlRouterProvider.otherwise('/app/usuarios');
    });
