angular.module('usuariosApp.services')

.factory('usuariosSvc', ['$q', 'ENDPOINT_USUARIOS', function($q, ENDPOINT_USUARIOS) {
        var getUsuarios = function() {
            var q = $q.defer();
            $.ajax({
                url: ENDPOINT_USUARIOS,
                dataType: 'json',
                data: {},
                type: 'GET',
                success: function(data) {
                		q.resolve(data)
                           },
                error: function(data) {
                		q.reject(data)
                }
            });
            
            return q.promise;
        };

        var deleteUsuario = function(id) {
            var q = $q.defer();
            $.ajax({
                url: ENDPOINT_USUARIOS + '/' + id,
                dataType: 'json',
                data: {
                },
                type: 'DELETE',
                success: function(data) {
                	console.log('apagou')
                    q.resolve(data) 
                },
                error: function(data) {
                	console.log('erro')
                	   q.resolve(data)
                }
            });

            return q.promise;
        };

        var usuariosSvc = {
            getUsuarios: getUsuarios,
            deleteUsuario: deleteUsuario
        }
        
        return usuariosSvc;
    }
])