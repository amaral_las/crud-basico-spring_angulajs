package com.amaral.usuarios.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.amaral.usuarios.models.Usuario;
import com.amaral.usuarios.repositorys.UsuarioRepository;;

@RestController
public class UsuarioCtrl {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@RequestMapping(value = "/usuarios", method = RequestMethod.GET)
	public ResponseEntity<List<Usuario>> todos(){
		List<Usuario> usuarios = this.usuarioRepository.findAll();
		if(usuarios.size() < 1) { 
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}else {
			return new ResponseEntity<List<Usuario>>(usuarios ,HttpStatus.OK);
		}
		
	}
	
	@RequestMapping(value = "/usuarios", method = RequestMethod.POST)
	public ResponseEntity<?> incluir(@Valid @RequestBody Usuario usuario){
			this.usuarioRepository.save(usuario);
			return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/usuarios/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> um(@PathVariable("id") Long id){
		Usuario usuario = this.usuarioRepository.findOne(id);
		if(usuario == null) { 
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}else {
			return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
		}
		
	}
	
	@RequestMapping(value = "/usuarios/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> alterar(@PathVariable("id") Long id, @Valid @RequestBody Usuario usuario){
		usuario.setId(id);
		this.usuarioRepository.save(usuario);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value = "/usuarios/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> excluir(@PathVariable("id") Long id){
		Usuario usuario = this.usuarioRepository.findOne(id);
		if(usuario == null) { 
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}else {
			this.usuarioRepository.delete(id);
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
	}

}
