angular.module('usuariosApp.controllers')

.controller('AppCtrl', function($rootScope, $scope, $stateParams, $window) {

    $scope.isBack = 'none';

    $scope.back = function() {
        $window.history.back();
    }
    $rootScope.$on('$locationChangeStart', function(event, next, current) {
         console.log(next.indexOf('#/app/usuarios-edit') != -1);
         if (next.indexOf('#/app/usuarios-create') != -1) {
             $scope.isBack = 'block';
        }else if (next.indexOf('#/app/usuarios-edit') != -1) {
             $scope.isBack = 'block';
        } else {
        		$scope.isBack = 'none';
        }
     });
})