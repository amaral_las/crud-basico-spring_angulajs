package com.amaral.usuarios.repositorys;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.amaral.usuarios.models.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario,Long> {
	
}
